using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Services.Impls.Actual;

namespace Plugins.PdAnalytics.Services
{
    public interface IInitializationPdService
    {
        string ServerUrl { get; }

        /// <summary>
        ///     Initializes countly instance
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="appKey"></param>
        /// <param name="deviceId"></param>
        void Begin(string serverUrl);

        /// <summary>
        ///     Initializes the Countly SDK with default values
        /// </summary>
        /// <param name="salt"></param>
        /// <param name="enablePost"></param>
        /// <param name="enableConsoleErrorLogging"></param>
        /// <param name="ignoreSessionCooldown"></param>
        /// <returns></returns>
        void SetDefaults(PdConfigModel configModel, SessionPdService sessionPdService);

        /// <summary>
        ///     Gets the base url to make requests to the Countly server.
        /// </summary>
        /// <returns></returns>
        string GetBaseUrl();
    }
}