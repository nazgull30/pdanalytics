namespace Plugins.PdAnalytics.Services
{
    public interface IViewPdService
    {
        void RecordOpenViewAsync(string name, bool hasSessionBegunWithView = false);
        void RecordCloseViewAsync(string name, bool hasSessionBegunWithView = false);
    }
}