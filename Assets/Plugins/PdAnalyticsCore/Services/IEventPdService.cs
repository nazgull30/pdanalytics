using System.Collections.Generic;
using System.Threading.Tasks;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;

namespace Plugins.PdAnalytics.Services
{
    public interface IEventPdService
    {
        void RecordEventAsync(PdEventModel @event, bool useNumberInSameSession = false);
        void RecordEventAsync(string key, bool useNumberInSameSession = false);

        void RecordEventAsync(string key, SegmentModel segmentation, bool useNumberInSameSession = false,
            int? count = 1, double? sum = 0, double? duration = null);
        
        void RecordEventAsync(string key, Dictionary<string, object> segmentation, bool useNumberInSameSession = false,
            int? count = 1, double? sum = 0, double? duration = null);

        /// <summary>
        ///     Reports all recorded view events to the server
        /// </summary>
        void ReportAllRecordedViewEventsAsync(bool addToRequestQueue = false);

        /// <summary>
        ///     Reports all recorded events to the server
        /// </summary>
        void ReportAllRecordedNonViewEventsAsync(bool addToRequestQueue = false);

        /// <summary>
        ///     Sends multiple events to the countly server. It expects a list of events as input.
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        void ReportMultipleEventsAsync(List<PdEventModel> events);

        /// <summary>
        ///     Reports a custom event to the Counlty server.
        /// </summary>
        /// <returns></returns>
        void ReportCustomEventAsync(string key,
            IDictionary<string, object> segmentation = null,
            int? count = 1, double? sum = null, double? duration = null);
    }
}