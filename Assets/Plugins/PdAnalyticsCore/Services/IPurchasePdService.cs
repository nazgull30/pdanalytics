using System;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;

namespace Plugins.PdAnalytics.Services
{
    public interface IPurchasePdService
    {
        void RecordPurchase(PdPurchaseModel model, Action<PdResponse> response);
    }
}