using System.Collections.Generic;

namespace Plugins.PdAnalytics.Services
{
    public interface IRemoteConfigPdService
    {
        Dictionary<string, object> Configs { get; }
        void InitConfig();
    }
}