namespace Plugins.PdAnalytics.Services
{
    public interface IStarRatingPdService
    {
        /// <summary>
        /// Sends app rating to the server.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="appVersion"></param>
        /// <param name="rating">Rating should be from 1 to 5</param>
        /// <returns></returns>
        void ReportStarRatingAsync(string platform, string appVersion, int rating);
    }
}