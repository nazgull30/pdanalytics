using System;
using System.Collections.Generic;
using System.Linq;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class UserDetailsPdService : IUserDetailsPdService
    {
        private readonly Dictionary<String, UserPropertyUpdate> _userPropertyUpdatesDict =
            new Dictionary<string, UserPropertyUpdate>();

        
        private readonly RequestPdHelper _requestPdHelper;
        private readonly IPdUtils _pdUtils;

        public UserDetailsPdService(RequestPdHelper requestPdHelper, IPdUtils pdUtils)
        {
            _requestPdHelper = requestPdHelper;
            _pdUtils = pdUtils;
        }
        
        /// <summary>
        /// Uploads only custom data. Doesn't update any other property except Custom Data.
        /// </summary>
        /// <returns></returns>
        public void SaveAsync()
        {
            var userPropertyUpdates = _userPropertyUpdatesDict.Values.ToList();
            _requestPdHelper.SendRequestAsync(_pdUtils.GetUserDetailsInputUrl(), RequestType.Put, userPropertyUpdates);
        }

        /// <summary>
        /// Sets value to key.
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Set(string key, object value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Set,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// Sets value to key, only if property was not defined before for this user.
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetOnce(string key, object value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.SetOnce,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// To increment value, for the specified key, on the server by 1.
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        public void Increment(string key)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Inc,
                    Value = 1
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// To increment value on server by provided value (if no value on server, assumes it is 0).
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void IncrementBy(string key, long value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Inc,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// To multiply value on server by provided value (if no value on server, assumes it is 0).
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Multiply(string key, long value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Mul,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// To store maximal value from the one on server and provided value (if no value on server, uses provided value).
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Max(string key, long value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Max,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }

        /// <summary>
        /// To store minimal value from the one on server and provided value (if no value on server, uses provided value).
        /// Doesn't report it to the server until save is called.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Min(string key, long value)
        {
            var update = new UserPropertyUpdate
            {
                Key = key,
                Update = new UserPropertyUpdate.Parameters
                {
                    Operator = UserPropertyUpdate.Operator.Min,
                    Value = value
                }
            };
            AddToUserPropertyUpdates(key, update);
        }
        
        private void AddToUserPropertyUpdates(string key, UserPropertyUpdate update)
        {
            if (_userPropertyUpdatesDict.ContainsKey(key))
            {
                var item = _userPropertyUpdatesDict.Select(x => x.Key).FirstOrDefault(x => x.Equals(key, StringComparison.OrdinalIgnoreCase));
                if (item != null)
                {
                    _userPropertyUpdatesDict.Remove(item);
                }
            }

            _userPropertyUpdatesDict.Add(key, update);
        }
        
        
    }
}