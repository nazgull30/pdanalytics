using System.Collections.Generic;
using Newtonsoft.Json;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Repositories.Impls;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class EventPdService : IEventPdService
    {
        private readonly PdConfigModel _pdConfigModel;
        private readonly RequestPdHelper _requestPdHelper;
        private readonly ViewEventRepository _viewEventRepo;
        private readonly NonViewEventRepository _nonViewEventRepo;
        private readonly EventNumberInSameSessionHelper _eventNumberInSameSessionHelper;
        private readonly IPdUtils _pdUtils;

        internal EventPdService(PdConfigModel pdConfigModel, IPdUtils pdUtils, RequestPdHelper requestPdHelper, 
            ViewEventRepository viewEventRepo, NonViewEventRepository nonViewEventRepo, EventNumberInSameSessionHelper eventNumberInSameSessionHelper)
        {
            _pdConfigModel = pdConfigModel;
            _requestPdHelper = requestPdHelper;
            _viewEventRepo = viewEventRepo;
            _nonViewEventRepo = nonViewEventRepo;
            _eventNumberInSameSessionHelper = eventNumberInSameSessionHelper;
            _pdUtils = pdUtils;
        }

        public void RecordEventAsync(PdEventModel @event, bool useNumberInSameSession = false)
        {
            if (_pdConfigModel.EnableFirstAppLaunchSegment)
            {
                AddFirstAppSegment(@event);   
            }

            if (@event.Key.Equals(PdEventModel.ViewEvent))
            {
                _viewEventRepo.Enqueue(@event);
            }
            else
            {
                _nonViewEventRepo.Enqueue(@event);
            }

            if (useNumberInSameSession)
            {
                _eventNumberInSameSessionHelper.IncreaseNumberInSameSession(@event);
            }
            
            ReportAllRecordedViewEventsAsync();
            ReportAllRecordedNonViewEventsAsync();
            
            // if (_viewEventRepo.Count >= _pdConfigModel.EventViewSendThreshold)
            //     await ReportAllRecordedViewEventsAsync();
            //
            // if (_nonViewEventRepo.Count >= _pdConfigModel.EventNonViewSendThreshold)
            //     await ReportAllRecordedNonViewEventsAsync();
        }
        
        
        public void RecordEventAsync(string key, bool useNumberInSameSession = false)
        {
            if (string.IsNullOrEmpty(key) && string.IsNullOrWhiteSpace(key))
            {
                return;
            }

            var @event = new PdEventModel(key);
            
            if (useNumberInSameSession)
            {
                _eventNumberInSameSessionHelper.IncreaseNumberInSameSession(@event);
            }
            
            RecordEventAsync(@event);
        }

        public void RecordEventAsync(string key, SegmentModel segmentation, bool useNumberInSameSession = false,
            int? count = 1, double? sum = 0, double? duration = null)
        {
            var dict = (Dictionary<string, object>) segmentation;
            RecordEventAsync(key, dict, useNumberInSameSession, count, sum, duration);
        }

        public void RecordEventAsync(string key, Dictionary<string, object> segmentation, bool useNumberInSameSession = false,
            int? count = 1, double? sum = 0, double? duration = null)
        {
            if (string.IsNullOrEmpty(key) && string.IsNullOrWhiteSpace(key))
            {
                return;     
            }
            
            var @event = new PdEventModel(key, segmentation, count, sum, duration);
            
            if (useNumberInSameSession)
            {
                _eventNumberInSameSessionHelper.IncreaseNumberInSameSession(@event);
            }
            
            RecordEventAsync(@event);
        }


        /// <summary>
        ///     Reports all recorded view events to the server
        /// </summary>
        public void ReportAllRecordedViewEventsAsync(bool addToRequestQueue = false)
        {
            if (_viewEventRepo.Models.Count == 0)
            {
                return;
            }

            var events = _viewEventRepo.Models;

            _requestPdHelper.SendRequestAsync(_pdUtils.GetEventInputUrl(), RequestType.Post, events, addToRequestQueue);

            //Even if res = false all events should be removed because responses are stored locally.
            _viewEventRepo.Clear();
        }
        
        
        
        /// <summary>
        ///     Reports all recorded events to the server
        /// </summary>
        public void ReportAllRecordedNonViewEventsAsync(bool addToRequestQueue = false)
        {
            if (_nonViewEventRepo.Models.Count == 0)
            {
                return;
            }

            var events = _nonViewEventRepo.Models;

            _requestPdHelper.SendRequestAsync(_pdUtils.GetEventInputUrl(), RequestType.Post, events, addToRequestQueue);
            //Even if res = false all events should be removed because responses are stored locally.
            _nonViewEventRepo.Clear();
        }

        /// <summary>
        ///     Sends multiple events to the countly server. It expects a list of events as input.
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        public void ReportMultipleEventsAsync(List<PdEventModel> events)
        {
            if (events == null || events.Count == 0)
                return;

            if (_pdConfigModel.EnableFirstAppLaunchSegment)
            {
                foreach (var evt in events)
                {
                    AddFirstAppSegment(evt);
                }       
            }

//            var currentTime = DateTime.UtcNow;
//            foreach (var evt in events)
//            {
//                SetTimeZoneInfo(evt, currentTime);
//            }

            var requestBody =
                new Dictionary<string, object>
                {
                    {
                        "events", JsonConvert.SerializeObject(events, Formatting.Indented,
                            new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore})
                    }
                };
            _requestPdHelper.SendRequestAsync(_pdUtils.GetEventInputUrl(), RequestType.Post, requestBody);
        }

        /// <summary>
        ///     Reports a custom event to the Counlty server.
        /// </summary>
        /// <returns></returns>
        public void ReportCustomEventAsync(string key,
            IDictionary<string, object> segmentation = null,
            int? count = 1, double? sum = null, double? duration = null)
        {
            if (string.IsNullOrEmpty(key) && string.IsNullOrWhiteSpace(key))
                return;

            var evt = new PdEventModel(key, segmentation, count, sum, duration);

            if (_pdConfigModel.EnableFirstAppLaunchSegment)
            {
                AddFirstAppSegment(evt);   
            }
//            SetTimeZoneInfo(evt, DateTime.UtcNow);

            var events = new List<PdEventModel> { evt };
            _requestPdHelper.SendRequestAsync(_pdUtils.GetEventInputUrl(), RequestType.Post, events, true);
        }

        private void AddFirstAppSegment(PdEventModel @event)
        {
            if (@event.Segmentation == null)
            {
                @event.Segmentation = new SegmentModel();
            }
            @event.Segmentation.Add(Constants.FirstAppLaunchSegment, FirstLaunchAppHelper.IsFirstLaunchApp);
        }
    }
}