using System;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class PurchasePdService : IPurchasePdService
    {
        private readonly IPdUtils _pdUtils;
        private readonly RequestPdHelper _requestPdHelper;

        public PurchasePdService(IPdUtils pdUtils, RequestPdHelper requestPdHelper)
        {
            _pdUtils = pdUtils;
            _requestPdHelper = requestPdHelper;
        }

        public void RecordPurchase(PdPurchaseModel model, Action<PdResponse> response)
        {
            model.AppVersion = Application.version;
           _requestPdHelper.PostAsync(_pdUtils.GetPurchaseInputUrl(),  model, response);
        }
    }
}