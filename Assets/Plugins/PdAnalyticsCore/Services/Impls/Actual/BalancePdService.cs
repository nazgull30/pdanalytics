using System;
using System.Collections.Generic;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class BalancePdService : IBalancePdService
    {
        private readonly IPdUtils _pdUtils;
        private readonly RequestPdHelper _requestPdHelper;

        public BalancePdService(IPdUtils pdUtils, RequestPdHelper requestPdHelper)
        {
            _pdUtils = pdUtils;
            _requestPdHelper = requestPdHelper;
        }

        public void RecordChangeVirtualCurrencyBalance(string virtualCurrency, long delta, string source, string sourceArgument)
        {
            var requestParams =
                new Dictionary<string, object>
                {
                    { "vc", virtualCurrency },
                    { "d", delta },
                    { "s", source },
                    { "arg", sourceArgument },
                    { "ts", DateTimeOffset.UtcNow.ToUnixTimeMilliseconds()},
                    { "appv", Application.version }
                };
            _requestPdHelper.SendRequestAsync(_pdUtils.GetBalanceInputUrl(), RequestType.Post, requestParams, true);
        }
    }
}