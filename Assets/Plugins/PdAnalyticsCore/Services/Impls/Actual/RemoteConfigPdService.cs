using System;
using System.Collections.Generic;
using System.Text;
using Plugins.iBoxDB;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Persistence.Entities;
using UnityEngine;
using UnityEngine.Networking;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class RemoteConfigPdService : IRemoteConfigPdService
    {
        private readonly RequestPdHelper _requestPdHelper;
        private readonly IPdUtils _pdUtils;
        private readonly Dao<ConfigEntity> _configDao;

        public  Dictionary<string, object> Configs { private set; get; } 

        private readonly StringBuilder _requestStringBuilder = new StringBuilder();
        
        public RemoteConfigPdService(RequestPdHelper requestPdHelper, IPdUtils pdUtils, Dao<ConfigEntity> configDao)
        {
            _requestPdHelper = requestPdHelper;
            _pdUtils = pdUtils;
            _configDao = configDao;
        }

        public void InitConfig()
        {
            var requestParams =
                new Dictionary<string, object>
                {
                    { "method", "fetch_remote_config" }
                };

            var url = BuildGetRequest(requestParams);

            void Response(PdResponse r)
            {
                if (r.IsSuccess)
                {
                    _configDao.RemoveAll();
                    var configEntity = new ConfigEntity {Id = _configDao.GenerateNewId(), Json = r.Data};
                    _configDao.Save(configEntity);
                    Configs = Converter.ConvertJsonToDictionary(r.Data);
                }
                else
                {
                    var allConfigs = _configDao.LoadAll();
                    if (allConfigs != null && allConfigs.Count > 0)
                    {
                        Configs = Converter.ConvertJsonToDictionary(allConfigs[0].Json);
                        Debug.Log("Configs: " + Configs.Count);
                    }
                }
            }

            _requestPdHelper.GetAsync(url, Response);
        }
        
        
        /// <summary>
        ///     Builds request URL using ServerUrl, AppKey, DeviceID and supplied queryParams parameters.
        ///     The data is appended in the URL.
        /// </summary>
        /// <param name="queryParams"></param>
        /// <returns></returns>
        private string BuildGetRequest(Dictionary<string, object> queryParams)
        {
            _requestStringBuilder.Clear();
            //Metrics added to each request
            foreach (var item in _pdUtils.GetAppKeyAndDeviceIdParams())
            {
                _requestStringBuilder.AppendFormat((item.Key != "app_key" ? "&" : string.Empty) + "{0}={1}",
                    UnityWebRequest.EscapeURL(item.Key), UnityWebRequest.EscapeURL(Convert.ToString(item.Value))); 
            }


            //Query params supplied for creating request
            foreach (var item in queryParams)
            {
                if (!string.IsNullOrEmpty(item.Key) && item.Value != null)
                {
                    _requestStringBuilder.AppendFormat("&{0}={1}", UnityWebRequest.EscapeURL(item.Key),
                        UnityWebRequest.EscapeURL(Convert.ToString(item.Value)));
                }
            }

            //Not sure if we need checksum here
            
//            if (!string.IsNullOrEmpty(_config.Salt))
//            {
//                // Create a SHA256   
//                using (var sha256Hash = SHA256.Create())
//                {
//                    var data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(_requestStringBuilder + _config.Salt));
//                    _requestStringBuilder.Insert(0, _countly.GetBaseUrl());
//                    return _requestStringBuilder.AppendFormat("&checksum256={0}", Impl.Countly.GetStringFromBytes(data)).ToString();
//                }
//            }
      
            _requestStringBuilder.Insert(0, _pdUtils.GetRemoteConfigOutputUrl());
            return _requestStringBuilder.ToString();
        }

    }
}