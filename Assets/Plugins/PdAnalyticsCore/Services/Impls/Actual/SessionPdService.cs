using System;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
	public class SessionPdService
	{
		private Timer _sessionTimer;
		public bool IsSessionInitiated { get; private set; }

		private DateTime _lastInputTime;

		private UserParams _userParams = new UserParams();

		private readonly PdConfigModel _configModel;
        private readonly IPdUtils _pdUtils;
        private readonly RequestPdHelper _requestPdHelper;
        private readonly EventNumberInSameSessionHelper _eventNumberInSameSessionHelper;
        private readonly string _androidId;

        public SessionPdService(PdConfigModel configModel, IPdUtils pdUtils,
            RequestPdHelper requestPdHelper,
            EventNumberInSameSessionHelper eventNumberInSameSessionHelper, string androidId)
        {
            _configModel = configModel;
            _pdUtils = pdUtils;
            _requestPdHelper = requestPdHelper;
            _eventNumberInSameSessionHelper = eventNumberInSameSessionHelper;
            _androidId = androidId;
        }

        public UserParams GetUserParams() => _userParams;

        /// <summary>
        /// Intializes the timer for extending session with specified interval
        /// </summary>
        private void InitSessionTimer()
		{
			if (_configModel.EnableManualSessionHandling) return;
			_sessionTimer = new Timer {Interval = _configModel.SessionDuration * 1000};
			_sessionTimer.Elapsed += SessionTimerOnElapsedAsync;
			_sessionTimer.AutoReset = true;
		}

		/// <summary>
		/// Extends the session after the session duration is elapsed
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="elapsedEventArgs"></param>
		private void SessionTimerOnElapsedAsync(object sender, ElapsedEventArgs elapsedEventArgs)
		{
			_requestPdHelper.ProcessQueue();
			var sessionOver = (DateTime.Now - _lastInputTime).TotalSeconds >= _configModel.SessionDuration;
			Debug.Log("SessionTimerOnElapsedAsync -> sessionOver:"  + sessionOver + ", IsSessionInitiated:" + IsSessionInitiated);
			if (IsSessionInitiated && sessionOver)
			{
				ExecuteEndSession();
			}
			else if(IsSessionInitiated)
			{
				if (!_configModel.EnableManualSessionHandling)
				{
					ExtendSession();
				}
			}
		}

		public void UpdateInputTime()
		{
			_lastInputTime = DateTime.Now;

			if (!IsSessionInitiated && _sessionTimer == null) //session was over
			{
				ExecuteBeginSession();
			}
		}

		public async void ExecuteBeginSession(string adId = "")
		{
			_requestPdHelper.ProcessQueue();
			FirstLaunchAppHelper.Process();

			//Session initiated
			IsSessionInitiated = true;
			_eventNumberInSameSessionHelper.RemoveAllEvents();

			var sessionModel = new PdSessionModel
			{
				Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
				AndroidId = _androidId,
				Metrics = PdMetricModel.Metrics
			};

            // var response =  await Task.Run(() =>_requests.GetAsync("https://api.ipify.org", ""));
            // if (response.IsSuccess)
            // {
            //     var ip = response.Data;
            //     _sessions
            // }
            
            //Start session timer
            if (!_configModel.EnableManualSessionHandling)
            {
	            InitSessionTimer();
	            _sessionTimer.Start();
            }

            var userUid = UserUidHelper.GetUserUid();
            if (userUid.HasValue)
            {
	            _userParams.UserUid = userUid.Value;
            }
            
            var task = Task.Run(async () =>
            {
	            var r = await _requestPdHelper.GetTaskAsync("https://api.ipify.org", false);
	            var ip = r.IsSuccess ? r.Data : string.Empty;
	            var beginSessionResponse = await _requestPdHelper.PostTaskAsync(_pdUtils.GetSessionInputUrl() + "/begin" + (ip != string.Empty ? "/" + ip : "-"), sessionModel, true, true);
	            if (!string.IsNullOrEmpty(beginSessionResponse.Data))
	            {
		            _userParams = JsonConvert.DeserializeObject<UserParams>(beginSessionResponse.Data);
	            } 
            });

            await task;
            if (!userUid.HasValue && _userParams.UserUid.HasValue)
            {
	            UserUidHelper.SaveUserUid(_userParams.UserUid.Value);
            }
		}

		public void ExecuteEndSession(bool disposeTimer = true)
		{
			_requestPdHelper.ProcessQueue();
			IsSessionInitiated = false;
			_eventNumberInSameSessionHelper.RemoveAllEvents();

			var sessionModel = new PdSessionModel
			{
				Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
				Metrics = PdMetricModel.Metrics
			};
			
			
			if (!_configModel.EnableManualSessionHandling)
			{
				//Do not extend session after session ends
				if (disposeTimer)
				{
					_sessionTimer.Stop();
					_sessionTimer.Dispose();
					_sessionTimer.Close();
					_sessionTimer = null;
				}
			}
			
			Task.Run(() => _requestPdHelper.PostTaskAsync(_pdUtils.GetSessionInputUrl() + "/end", sessionModel, true, true));
		}
		
		/// <summary>
		/// Extends a session by another 60 seconds
		/// </summary>
		private void ExtendSession()
		{
			var sessionModel = new PdSessionModel
			{	
				Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds(),
			};

			Task.Run(() => _requestPdHelper.PostTaskAsync(_pdUtils.GetSessionInputUrl() + "/extend", sessionModel));
		}
		
		
		public class UserParams
		{
			[JsonProperty("user_uid")]
			public long? UserUid { set; get; }
			[JsonProperty("location_uid")]
			public long? LocationUid { set; get; }
		}
	}
}