using System;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class InitializationPdService : IInitializationPdService
    {
        public string ServerUrl { get; private set; }

        /// <summary>
        ///     Initializes countly instance
        /// </summary>
        /// <param name="serverUrl"></param>
        /// <param name="appKey"></param>
        /// <param name="deviceId"></param>
        public void Begin(string serverUrl)
        {
            ServerUrl = serverUrl;

            if (string.IsNullOrEmpty(ServerUrl))
                throw new ArgumentNullException(serverUrl, "Server URL is required.");

            //ConsentGranted = consentGranted;
        }

        /// <summary>
        ///     Initializes the Countly SDK with default values
        /// </summary>
        /// <param name="salt"></param>
        /// <param name="enablePost"></param>
        /// <param name="enableConsoleErrorLogging"></param>
        /// <param name="ignoreSessionCooldown"></param>
        /// <returns></returns>
        public void SetDefaults(PdConfigModel configModel, SessionPdService sessionPdService)
        {
            if (!configModel.EnableManualSessionHandling)
            {
                //Start Session and enable push notification
                sessionPdService.ExecuteBeginSession();
//                if (!result.IsSuccess) Debug.LogError("BeginSessionAsync error: " + result);
            }
        }

        /// <summary>
        ///     Gets the base url to make requests to the Countly server.
        /// </summary>
        /// <returns></returns>
        public string GetBaseUrl()
        {
            return string.Format(ServerUrl[ServerUrl.Length - 1] == '/' ? "{0}i?" : "{0}/i?", ServerUrl);
        }
    }
}