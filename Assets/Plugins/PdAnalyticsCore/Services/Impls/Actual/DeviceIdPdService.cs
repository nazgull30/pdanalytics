using Plugins.PdAnalytics.Helpers;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class DeviceIdPdService : IDeviceIdPdService
    {

        private readonly SessionPdService _sessionPdService;
        private readonly RequestPdHelper _requestPdHelper;
        private readonly IEventPdService _eventPdService;
        private readonly IPdUtils _pdUtils;

        public DeviceIdPdService(SessionPdService sessionPdService, 
            RequestPdHelper requestPdHelper, IEventPdService eventPdService, IPdUtils pdUtils)
        {
            _sessionPdService = sessionPdService;
            _requestPdHelper = requestPdHelper;
            _eventPdService = eventPdService;
            _pdUtils = pdUtils;
        }

        public string DeviceId { get; private set; }

        public void InitDeviceId(string deviceId = null)
        {
            //**Priority is**
            //Cached DeviceID (remains even after after app kill)
            //Static DeviceID (only when the app is running either backgroun/foreground)
            //User provided DeviceID
            //Generate Random DeviceID
            var storedDeviceId = PlayerPrefs.GetString("DeviceID");
            DeviceId = !_pdUtils.IsNullEmptyOrWhitespace(storedDeviceId)
                ? storedDeviceId
                : !_pdUtils.IsNullEmptyOrWhitespace(DeviceId)
                    ? DeviceId
                    : !_pdUtils.IsNullEmptyOrWhitespace(deviceId)
                        ? deviceId : _pdUtils.GetUniqueDeviceId();
            
            //Set DeviceID in Cache if it doesn't already exists in Cache
            if (_pdUtils.IsNullEmptyOrWhitespace(storedDeviceId))
                PlayerPrefs.SetString(Constants.DeviceIDKey, DeviceId);
        }
        
        /// <summary>
        /// Changes Device Id.
        /// Adds currently recorded but not queued events to request queue.
        /// Clears all started timed-events
        /// Ends cuurent session with old Device Id.
        /// Begins a new session with new Device Id
        /// </summary>
        /// <param name="deviceId"></param>
        public void ChangeDeviceIdAndEndCurrentSessionAsync(string deviceId)
        {
            //Ignore call if new and old device id are same
            // if (DeviceId == deviceId)
            //     return new PdResponse();

            //Add currently recorded but not queued view events to request queue-----------------------------------
            _eventPdService.ReportAllRecordedViewEventsAsync(true);

            //Add currently recorded but not queued non view events to request queue-----------------------------------
            _eventPdService.ReportAllRecordedNonViewEventsAsync(true);
            
            //Ends current session
            //Do not dispose timer object
            _sessionPdService.ExecuteEndSession(false);

            //Update device id
            UpdateDeviceId(deviceId);

            //Begin new session with new device id
            //Do not initiate timer again, it is already initiated
            _sessionPdService.ExecuteBeginSession();
        }

        /// <summary>
        /// Changes DeviceId. 
        /// Continues with the current session.
        /// Merges data for old and new Device Id. 
        /// </summary>
        /// <param name="deviceId"></param>
        public void ChangeDeviceIdAndMergeSessionDataAsync(string deviceId)
        {
            //Ignore call if new and old device id are same
            // if (DeviceId == deviceId)
            //     return false;

            //Keep old device id
            var oldDeviceId = DeviceId;

            //Update device id
            UpdateDeviceId(deviceId);

            //UNCOMMENT IF NEEDED
            
            //Merge user data for old and new device
            // var requestParams =
            //    new Dictionary<string, object>
            //    {
            //             { "old_device_id", oldDeviceId }
            //    };
            //
            // _requestPdHelper.SendRequestAsync(_pdUtils.GetDeviceInputUrl(), RequestType.Put, requestParams);
        }
        
        /// <summary>
        /// Updates Device ID both in app and in cache
        /// </summary>
        /// <param name="newDeviceId"></param>
        public void UpdateDeviceId(string newDeviceId)
        {
            //Change device id
            DeviceId = newDeviceId;

            //Updating Cache
            PlayerPrefs.SetString(Constants.DeviceIDKey, DeviceId);
        }
        
        // private void SendAdvertisementId()
        // {
        //     Application.RequestAdvertisingIdentifierAsync((adId, trackingEnabled, error) =>
        //     {
        //         Debug.Log("[DeviceIdPdService] RequestAdvertisingIdentifierAsync -> adId: " + adId + ", trackingEnabled: " 
        //                   + trackingEnabled + ", error: " + error);
        //         var requestParams =
        //             new Dictionary<string, object>
        //             {
        //                 { "ad_id", adId }
        //             };
        //         _requestPdHelper.SendRequestAsync(_pdUtils.GetDeviceInputUrl() + "/adid", RequestType.Put, requestParams);
        //     });
        //     
        // }

    }
}