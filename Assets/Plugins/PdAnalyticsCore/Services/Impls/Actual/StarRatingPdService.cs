using System;
using System.Collections.Generic;
using Plugins.PdAnalytics.Models;

namespace Plugins.PdAnalytics.Services.Impls.Actual
{
    public class StarRatingPdService : IStarRatingPdService
    {

        private readonly IEventPdService _eventPdService;

        public StarRatingPdService(IEventPdService eventPdService)
        {
            _eventPdService = eventPdService;
        }


        /// <summary>
        /// Sends app rating to the server.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="appVersion"></param>
        /// <param name="rating">Rating should be from 1 to 5</param>
        /// <returns></returns>
        public void ReportStarRatingAsync(string platform, string appVersion, int rating)
        {
            if (rating < 1 || rating > 5)
            {
                return;
            }

            var segment =
                new StarRatingSegment
                {
                    Platform = platform,
                    AppVersion = appVersion,
                    Rating = rating,
                };

            _eventPdService.ReportCustomEventAsync(
                PdEventModel.StarRatingEvent, segment.ToDictionary(),
                null);
        }
        
        
        /// <summary>
        /// Custom Segmentation for Star Rating event.
        /// </summary>
        [Serializable]
        struct StarRatingSegment
        {
            public string Platform { get; set; }
            public string AppVersion { get; set; }
            public int Rating { get; set; }
            
            public IDictionary<string, object> ToDictionary()
            {
                return new Dictionary<string, object>()
                {
                    {"platform", Platform},
                    {"app_version", AppVersion},
                    {"rating", Rating},
                };
            }
            
        }

    }
}