using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Services.Impls.Actual;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class InitializationPdServiceWrapper : IInitializationPdService
    {
        public string ServerUrl { get; private set; }
        public void Begin(string serverUrl)
        {
            Debug.Log("[InitializationPdServiceWrapper] Begin, serverUrl: " + serverUrl);
            ServerUrl = serverUrl;
        }

        public void SetDefaults(PdConfigModel configModel, SessionPdService sessionPdService)
        {
            Debug.Log("[InitializationPdServiceWrapper] SetDefaults, model: \n" + configModel);
        }

        public string GetBaseUrl()
        {
            Debug.Log("[InitializationPdServiceWrapper] GetBaseUrl");
            return string.Empty;
        }
    }
}