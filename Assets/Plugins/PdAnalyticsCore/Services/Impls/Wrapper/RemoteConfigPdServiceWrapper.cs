using System.Collections.Generic;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class RemoteConfigPdServiceWrapper : IRemoteConfigPdService
    {
        public Dictionary<string, object> Configs { get; }
        
        public void InitConfig()
        {
            Debug.Log("[RemoteConfigPdServiceWrapper] InitConfig");
        }
    }
}