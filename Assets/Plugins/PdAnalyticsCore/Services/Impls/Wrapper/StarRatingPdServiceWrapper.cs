using System.Threading.Tasks;
using Plugins.PdAnalytics.Helpers;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class StarRatingPdServiceWrapper : IStarRatingPdService
    {
        public void ReportStarRatingAsync(string platform, string appVersion, int rating)
        {
            Debug.Log("[StarRatingPdServiceWrapper] ReportStarRatingAsync, platform: " + platform + ", appVersion: " + appVersion + ", rating: " + rating);
        }
    }
}