using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class ViewPdServiceWrapper : IViewPdService
    {
        public void RecordOpenViewAsync(string name, bool hasSessionBegunWithView = false)
        {
            Debug.Log("[ViewPdServiceWrapper] ReportOpenViewAsync, name: " + name + ", hasSessionBegunWithView: " + hasSessionBegunWithView);
        }

        public void RecordCloseViewAsync(string name, bool hasSessionBegunWithView = false)
        {
            Debug.Log("[ViewPdServiceWrapper] ReportCloseViewAsync, name: " + name + ", hasSessionBegunWithView: " + hasSessionBegunWithView);
        }
    }
}