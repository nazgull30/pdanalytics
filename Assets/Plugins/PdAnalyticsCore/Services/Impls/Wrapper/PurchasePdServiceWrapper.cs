using System;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class PurchasePdServiceWrapper : IPurchasePdService
    {
        public void RecordPurchase(PdPurchaseModel model, Action<PdResponse> response)
        {
            model.AppVersion = Application.version;
            Debug.Log("[PurchasePdServiceWrapper] RecordPurchase: " + model);
        }
    }
}