using System.Collections.Generic;
using System.Linq;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class EventPdServiceWrapper : IEventPdService
    {
        public void RecordEventAsync(PdEventModel @event, bool useNumberInSameSession = false)
        {
            Debug.Log("[EventPdServiceWrapper] RecordEventAsync: " + @event);
        }

        public void RecordEventAsync(string key, bool useNumberInSameSession = false)
        {
            Debug.Log("[EventPdServiceWrapper] RecordEventAsync, key: " + key);
        }

        public void RecordEventAsync(string key, SegmentModel segmentModel, bool useNumberInSameSession = false, int? count = 1, double? sum = 0, double? duration = null)
        {
            
            Debug.Log("[EventPdServiceWrapper] RecordEventAsync, key: " + key + ", segments: " + segmentModel + ", count: " + count + ", sum: " + sum + ", dur: " + duration);
        }

        public void RecordEventAsync(string key, Dictionary<string, object> segmentation, bool useNumberInSameSession = false, int? count = 1, double? sum = 0,
            double? duration = null)
        {
            Debug.Log("[EventPdServiceWrapper] RecordEventAsync, key: " + key + ", segments: " + segmentation + ", count: " + count + ", sum: " + sum + ", dur: " + duration);
        }

        public void ReportAllRecordedViewEventsAsync(bool addToRequestQueue = false)
        {
            Debug.Log("[EventPdServiceWrapper] ReportAllRecordedViewEventsAsync, addToRequestQueue: " + addToRequestQueue);
        }

        public void ReportAllRecordedNonViewEventsAsync(bool addToRequestQueue = false)
        {
            Debug.Log("[EventPdServiceWrapper] ReportAllRecordedNonViewEventsAsync, addToRequestQueue: " + addToRequestQueue);
        }

        public void ReportMultipleEventsAsync(List<PdEventModel> events)
        {
            Debug.Log("[EventPdServiceWrapper] ReportMultipleEventsAsync, events: " + events.Count);
        }

        public void ReportCustomEventAsync(string key, IDictionary<string, object> segmentation, int? count, double? sum = null,
            double? duration = null)
        {
            var segmentStr = string.Join(";", segmentation.Select(x => x.Key + "=" + x.Value).ToArray());
            Debug.Log("[EventPdServiceWrapper] ReportCustomEventAsync, key: " + key + ", segments: \n" + segmentStr + "\n, int: " + count + ", sum: " + sum + ", dur: " + duration);
        }
    }
}