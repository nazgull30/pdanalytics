using System.Threading.Tasks;
using Plugins.PdAnalytics.Helpers;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class UserDetailsPdServiceWrapper : IUserDetailsPdService
    {
        public void SaveAsync()
        {
            Debug.Log("[UserDetailsPdServiceWrapper] SaveAsync");
        }

        public void Set(string key, object value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] Set, key: " + key + ", value: " + value);
        }

        public void SetOnce(string key, object value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] SetOnce, key: " + key + ", value: " + value);
        }

        public void Increment(string key)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] Increment, key: " + key);
        }

        public void IncrementBy(string key, long value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] IncrementBy, key: " + key + ", value: " + value);
        }

        public void Multiply(string key, long value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] Multiply, key: " + key + ", value: " + value);
        }

        public void Max(string key, long value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] Max, key: " + key + ", value: " + value);
        }

        public void Min(string key, long value)
        {
            Debug.Log("[UserDetailsPdServiceWrapper] Min, key: " + key + ", value: " + value);
        }
    }
}