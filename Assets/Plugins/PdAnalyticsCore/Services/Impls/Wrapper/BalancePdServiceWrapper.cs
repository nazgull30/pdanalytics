using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class BalancePdServiceWrapper : IBalancePdService
    {
        public void RecordChangeVirtualCurrencyBalance(string virtualCurrency, long delta, string source, string sourceArgument)
        {
            Debug.Log("[BalancePdServiceWrapper] RecordChangeVirtualCurrencyBalance, virtualCurrency: " + virtualCurrency + 
                      ", delta: " + delta + ", source: " + source + ", sourceArgument: " + sourceArgument);
        }
    }
}