using System.Collections.Generic;
using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class CrushReportsPdServiceWrapper : ICrushReportsPdService
    {
        public void LogCallback(string message, string stackTrace, LogType type)
        {
            
        }

        public void SendCrashReportAsync(string message, string stackTrace, LogType type, IDictionary<string, object> segments = null,
            bool nonfatal = true)
        {
            Debug.Log("[CrushReportsPdServiceWrapper] Send crush report async, message: \n" + message + "\n" +
                      stackTrace + "\n " + type);
        }

        public void SendCrashReportAsync(string message, string stackTrace, LogType type, IDictionary<string, object> segments = null)
        {
            Debug.Log("[CrushReportsPdServiceWrapper] Send crush report async, message: \n" + message + "\n" +
                      stackTrace + "\n " + type);
        }

        public void AddBreadcrumbs(string value)
        {
            Debug.Log("[CrushReportsPdServiceWrapper] Add breadcrumbs, value: " + value);
        }
    }
}