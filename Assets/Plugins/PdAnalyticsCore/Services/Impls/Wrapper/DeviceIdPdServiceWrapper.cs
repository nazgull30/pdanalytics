using UnityEngine;

namespace Plugins.PdAnalytics.Services.Impls.Wrapper
{
    public class DeviceIdPdServiceWrapper : IDeviceIdPdService
    {
        public string DeviceId { get; }
        public void InitDeviceId(string deviceId = null)
        {
            Debug.Log("[DeviceIdPdServiceWrapper] init device id: " + deviceId);
        }

        public void ChangeDeviceIdAndEndCurrentSessionAsync(string deviceId)
        {
            Debug.Log("[DeviceIdPdServiceWrapper] ChangeDeviceIdAndEndCurrentSessionAsync, device id: " + deviceId);
        }

        public void ChangeDeviceIdAndMergeSessionDataAsync(string deviceId)
        {
            Debug.Log("[DeviceIdPdServiceWrapper] ChangeDeviceIdAndMergeSessionDataAsync, device id: " + deviceId);
        }

        public void UpdateDeviceId(string newDeviceId)
        {
            Debug.Log("[DeviceIdPdServiceWrapper] UpdateDeviceId, device id: " + newDeviceId);
        }
    }
}