namespace Plugins.PdAnalytics.Services.Impls
{
    public interface IBalancePdService
    {
        void RecordChangeVirtualCurrencyBalance(string virtualCurrency, long delta, string source, string sourceArgument);
    }
}