namespace Plugins.PdAnalytics.Input
{
	public interface IInputObserver
	{
		bool HasInput { get; }
	}
}