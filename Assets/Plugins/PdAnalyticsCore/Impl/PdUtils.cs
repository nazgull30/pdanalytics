using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Plugins.PdAnalytics.Impl
{
    public class PdUtils : IPdUtils
    {
        private static readonly StringBuilder Builder = new StringBuilder();

        private readonly PdAnalytics _pdAnalytics;

        public PdUtils(PdAnalytics pdAnalytics)
        {
            _pdAnalytics = pdAnalytics;
        }

        public string GetUniqueDeviceId()
        {
#if UNITY_IOS
            return UnityEngine.iOS.Device.advertisingIdentifier;
#else
            return SystemInfo.deviceUniqueIdentifier;
#endif
        }

        public string GetSessionInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}sessions"
                    : "{0}/sessions",
                _pdAnalytics.Initialization.ServerUrl);
        }

        public string GetEventInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}events"
                    : "{0}/events",
                _pdAnalytics.Initialization.ServerUrl);
        }

        public string GetBalanceInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}balances"
                    : "{0}/balances",
                _pdAnalytics.Initialization.ServerUrl);
        }

        public string GetDeviceInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}devices"
                    : "{0}/devices",
                _pdAnalytics.Initialization.ServerUrl);
        }

        public string GetCrushInputUrl()
        {
            return string.Empty;
        }

        public string GetUserDetailsInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}userprops"
                    : "{0}/userprops",
                _pdAnalytics.Initialization.ServerUrl);
        }

        public string GetPurchaseInputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}purchases"
                    : "{0}/purchases",
                _pdAnalytics.Initialization.ServerUrl);
        }

        /// <summary>
        ///     Gets the base url to make remote configrequests to the Countly server.
        /// </summary>
        /// <returns></returns>
        public string GetRemoteConfigOutputUrl()
        {
            return string.Format(
                _pdAnalytics.Initialization.ServerUrl[_pdAnalytics.Initialization.ServerUrl.Length - 1] == '/'
                    ? "{0}o/sdk?"
                    : "{0}/o/sdk?",
                _pdAnalytics.Initialization.ServerUrl);
        }

        /// <summary>
        ///     Gets the least set of paramas required to be sent along with each request.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetBaseParams()
        {
            var baseParams = new Dictionary<string, object>
            {
                {"device_id", _pdAnalytics.Device.DeviceId},
            };

            var userParams = _pdAnalytics.GetUserParams();
            baseParams.Add("user_uid", userParams.UserUid.HasValue ? userParams.UserUid.Value.ToString() : "-");
            baseParams.Add("location_uid", userParams.LocationUid.HasValue ? userParams.LocationUid.Value.ToString() : "-");

            return baseParams;
        }

        /// <summary>
        ///     Gets the least set of app key and device id required to be sent along with remote config request,
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetAppKeyAndDeviceIdParams()
        {
            return new Dictionary<string, object>
            {
                {"device_id", _pdAnalytics.Device.DeviceId}
            };
        }

        public bool IsNullEmptyOrWhitespace(string input)
        {
            return string.IsNullOrEmpty(input) || string.IsNullOrWhiteSpace(input);
        }

        /// <summary>
        ///     Validates the picture format. The Countly server supports a specific set of formats only.
        /// </summary>
        /// <param name="pictureUrl"></param>
        /// <returns></returns>
        public bool IsPictureValid(string pictureUrl)
        {
            if (!string.IsNullOrEmpty(pictureUrl) && pictureUrl.Contains("?"))
                pictureUrl = pictureUrl.Split(new[] {'?'}, StringSplitOptions.RemoveEmptyEntries)[0];

            return string.IsNullOrEmpty(pictureUrl)
                   || pictureUrl.EndsWith(".png")
                   || pictureUrl.EndsWith(".jpg")
                   || pictureUrl.EndsWith(".jpeg")
                   || pictureUrl.EndsWith(".gif");
        }

        public string GetStringFromBytes(byte[] bytes)
        {
            for (var i = 0; i < bytes.Length; i++) Builder.Append(bytes[i].ToString("x2"));
            return Builder.ToString();
        }
    }
}