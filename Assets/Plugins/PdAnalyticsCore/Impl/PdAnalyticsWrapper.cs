using System;
using Plugins.PdAnalytics.Input;
using Plugins.PdAnalytics.Services;
using Plugins.PdAnalytics.Services.Impls;
using Plugins.PdAnalytics.Services.Impls.Wrapper;
using UnityEngine;

namespace Plugins.PdAnalytics.Impl
{
    public class PdAnalyticsWrapper : MonoBehaviour, IPdAnalytics
    {
        public ICrushReportsPdService CrushReports { get; private set; }

        public IDeviceIdPdService Device { get; private set; }

        public IEventPdService Events { get; private set; }

        public IInitializationPdService Initialization { get; private set; }

        public IOptionalParametersPdService OptionalParameters { get; private set; }

        public IRemoteConfigPdService RemoteConfigs { get; private set; }

        public IStarRatingPdService StarRating { get; private set; }

        public IUserDetailsPdService UserDetails { get; private set; }

        public IViewPdService Views { get; private set; }

        public IPurchasePdService Purchases { get; private set; }

        public IBalancePdService Balance { get; private set; }

        public void ReportAll()
        {
            Events.ReportAllRecordedViewEventsAsync();
            Events.ReportAllRecordedNonViewEventsAsync();
            UserDetails.SaveAsync();
        }

        private IInputObserver _inputObserver;

        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }

        private void Start()
        {
            CrushReports = new CrushReportsPdServiceWrapper();
            Device = new DeviceIdPdServiceWrapper();
            Events = new EventPdServiceWrapper();
            Initialization = new InitializationPdServiceWrapper();
            OptionalParameters = new OptionalParametersPdServiceWrapper();
            RemoteConfigs = new RemoteConfigPdServiceWrapper();
            StarRating = new StarRatingPdServiceWrapper();
            UserDetails = new UserDetailsPdServiceWrapper();
            Views = new ViewPdServiceWrapper();
            Purchases = new PurchasePdServiceWrapper();
            Balance = new BalancePdServiceWrapper();
            _inputObserver = InputObserverResolver.Resolve();
        }

        private void Update()
        {
            CheckLastInput();
        }

        private void CheckLastInput()
        {
            if(!_inputObserver.HasInput)
                return;
            //Debug.Log($"[PdWrapper] Last touch date: " + DateTime.Now);
        }
    }
}