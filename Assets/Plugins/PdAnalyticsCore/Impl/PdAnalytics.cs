using System.Collections;
using iBoxDB.LocalServer;
using Plugins.iBoxDB;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Input;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence;
using Plugins.PdAnalytics.Persistence.Dao;
using Plugins.PdAnalytics.Persistence.Entities;
using Plugins.PdAnalytics.Persistence.Repositories;
using Plugins.PdAnalytics.Persistence.Repositories.Impls;
using Plugins.PdAnalytics.Services;
using Plugins.PdAnalytics.Services.Impls;
using Plugins.PdAnalytics.Services.Impls.Actual;
using UnityEngine;

namespace Plugins.PdAnalytics.Impl
{
    public class PdAnalytics : MonoBehaviour, IPdAnalytics
    {

        public PdAuthModel Auth;
        public PdConfigModel Config;
        public static PdAnalytics Instance { get; internal set; }

        public ICrushReportsPdService CrushReports { get; private set; }

        public IDeviceIdPdService Device { get; private set; }

        public IEventPdService Events { get; private set; }

        public IInitializationPdService Initialization { get; private set; }

        public IOptionalParametersPdService OptionalParameters { get; private set; }

        public IRemoteConfigPdService RemoteConfigs { get; private set; }
        
        public IStarRatingPdService StarRating { get; private set; }

        public IUserDetailsPdService UserDetails { get; private set; }

        public IViewPdService Views { get; private set; }
        
        public IPurchasePdService Purchases { get; private set; }
        
        public IBalancePdService Balance { get; private set; }

        public void ReportAll()
        {
            Events.ReportAllRecordedViewEventsAsync();
            Events.ReportAllRecordedNonViewEventsAsync();
        }

        private IInputObserver _inputObserver;

        private DB _db;    

        private bool _logSubscribed;

        private SessionPdService _sessions;

        public SessionPdService.UserParams GetUserParams() => _sessions.GetUserParams();

        private string _androidId;

        private const long DbNumber = 4;

        /// <summary>
        ///     Initialize SDK at the start of your app
        /// </summary>
        private void Start()
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;

            _androidId = AndroidIdHelper.GetAndroidId();
            
            _db = PdBoxDbHelper.BuildDatabase(DbNumber);
            var auto = _db.Open();
            var requestDao = new Dao<RequestEntity>(auto, EntityType.Requests.ToString());
            var viewEventDao = new Dao<EventEntity>(auto, EntityType.ViewEvents.ToString());
            var nonViewEventDao = new Dao<EventEntity>(auto, EntityType.NonViewEvents.ToString());
            var configDao = new Dao<ConfigEntity>(auto, EntityType.Configs.ToString());
            
            var viewSegmentDao = new SegmentDao(auto, EntityType.ViewEventSegments.ToString());
            var nonViewSegmentDao = new SegmentDao(auto, EntityType.NonViewEventSegments.ToString());


            var requestRepo = new RequestRepository(requestDao);
            var eventViewRepo = new ViewEventRepository(viewEventDao, viewSegmentDao);
            var eventNonViewRepo = new NonViewEventRepository(nonViewEventDao, nonViewSegmentDao);
            var eventNrInSameSessionDao = new EventNumberInSameSessionDao(auto, EntityType.EventNumberInSameSessions.ToString());

            requestRepo.Initialize();
            eventViewRepo.Initialize();
            eventNonViewRepo.Initialize();

            var eventNumberInSameSessionHelper = new EventNumberInSameSessionHelper(eventNrInSameSessionDao);
            
            Init(requestRepo, eventViewRepo, eventNonViewRepo, configDao, eventNumberInSameSessionHelper);

            Initialization.Begin(Auth.ServerUrl);
            Device.InitDeviceId();

            Initialization.SetDefaults(Config, _sessions);
        }

        private void Init(RequestRepository requestRepo, ViewEventRepository viewEventRepo, 
            NonViewEventRepository nonViewEventRepo, Dao<ConfigEntity> configDao, 
            EventNumberInSameSessionHelper eventNumberInSameSessionHelper)
        {
            var pdUtils = new PdUtils(this);
            var requests = new RequestPdHelper(Config, pdUtils, requestRepo);
            
            OptionalParameters = new OptionalParametersPdService();
            Initialization = new InitializationPdService();
            _sessions = new SessionPdService(Config, pdUtils, requests, eventNumberInSameSessionHelper, _androidId);
            CrushReports = new CrushReportsPdService(Config, pdUtils, requests);
            Events = new EventPdService(Config, pdUtils, requests, viewEventRepo, nonViewEventRepo, eventNumberInSameSessionHelper);
            Device = new DeviceIdPdService(_sessions, requests, Events, pdUtils);
            
            RemoteConfigs = new RemoteConfigPdService(requests, pdUtils, configDao);
            
            StarRating = new StarRatingPdService(Events);
            UserDetails = new UserDetailsPdService(requests, pdUtils);
            Views = new ViewPdService(Events);
            Purchases = new PurchasePdService(pdUtils, requests);
            Balance = new BalancePdService(pdUtils, requests);
            
            _inputObserver = InputObserverResolver.Resolve();
        }

        
        /// <summary>
        ///     End session on application close/quit
        /// </summary>
        private void OnApplicationQuit()
        {
            Debug.Log("[PdAnalytics] OnApplicationQuit");
            if (_sessions != null && _sessions.IsSessionInitiated && !Config.EnableManualSessionHandling)
            {
                ReportAll();
                _sessions.ExecuteEndSession();
            }
            _db?.Close();
        }

        private async void OnApplicationFocus(bool hasFocus)
        {
            Debug.Log("[PdAnalytics] OnApplicationFocus: " + hasFocus);
            if (hasFocus)
            {
                SubscribeAppLog();
            }
            else
            {
                HandleAppPauseOrFocus();
            }
        }

        private async void OnApplicationPause(bool pauseStatus)
        {
            Debug.Log("[PdAnalytics] OnApplicationPause: " + pauseStatus);
            if (pauseStatus)
            {
                HandleAppPauseOrFocus();   
            }
            else
            {
                SubscribeAppLog();   
            }
        }

        private async void HandleAppPauseOrFocus()
        {
            UnsubscribeAppLog();
            if (_sessions != null && _sessions.IsSessionInitiated)
            {
                ReportAll();
            }
        }

        // Whenever app is enabled
        private void OnEnable()
        {
            SubscribeAppLog();
        }

        // Whenever app is disabled
        private void OnDisable()
        {
            UnsubscribeAppLog();
        }

        private void LogCallback(string condition, string stackTrace, LogType type)
        {
//            Debug.Log("[PdAnalytics] " + type + "," + condition + "\n " + stackTrace);
            CrushReports?.LogCallback(condition, stackTrace, type);
        }


        private void SubscribeAppLog()
        {
            if (_logSubscribed) return;
            Application.logMessageReceived += LogCallback;
            _logSubscribed = true;
        }

        private void UnsubscribeAppLog()
        {
            if (!_logSubscribed) return;
            Application.logMessageReceived -= LogCallback;
            _logSubscribed = false;
        }

        private void Update()
        {
            CheckInputEvent();
        }

        private void CheckInputEvent()
        {
            if(_inputObserver == null || !_inputObserver.HasInput)
                return;
            _sessions?.UpdateInputTime();
        }

        private void InternalStartCoroutine(IEnumerator enumerator)
        {
            StartCoroutine(enumerator);
        }
    }
}