using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Repositories;
using UnityEngine;
using UnityEngine.Networking;

namespace Plugins.PdAnalytics.Helpers
{
    public class RequestPdHelper
    {
        private readonly PdConfigModel _config;
        private readonly IPdUtils _pdUtils;
        private readonly RequestRepository _requestRepo;

        internal RequestPdHelper(PdConfigModel config, IPdUtils pdUtils, RequestRepository requestRepo)
        {
            _config = config;
            _pdUtils = pdUtils;
            _requestRepo = requestRepo;
        }

        private void AddRequestToQueue(PdRequestModel request)
        {
            if (_requestRepo.Count == _config.StoredRequestLimit)
                _requestRepo.Dequeue();

            _requestRepo.Enqueue(request);
        }

//        private PdRequestModel GetRequestFromQueue()
//        {
//            return _totalRequests.Peek();
//        }

        internal void ProcessQueue()
        {
            var requests = _requestRepo.Models.ToArray();
            Debug.Log("[RequestPdHelper] Process queue, requests: " + requests.Length);
            foreach (var reqModel in requests)
            {
                ProcessRequest(reqModel);
                _requestRepo.Dequeue();
//                var isProcessed = false;
//                var retryCount = 0;
//                while (!isProcessed && retryCount < 3)
//                {
//                    try
//                    {
//                        ProcessRequest(reqModel);
//                        isProcessed = true;
//                    }
//                    catch
//                    {
//                        retryCount++;
//                        isProcessed = false;
//                    }
//                    finally
//                    {
//                        if (isProcessed) _requestRepo.Dequeue();
//                    }                   
//                }
            }
        }

        private void ProcessRequest(PdRequestModel model, bool addToRequestQueue = false)
        {
            if(model == null)
                return;
            ProcessRequest(model.RequestType, model.RequestUrl, model.RequestData, addToRequestQueue);
        }
        
        private void ProcessRequest(RequestType requestType, string requestUrl, string requestData, 
             bool addToRequestQueue = false)
        {
            switch (requestType)
            {
                case RequestType.Get:
                    GetAsync(requestUrl, null, addToRequestQueue);
                    break;
                case RequestType.Post:
                    PostAsync(requestUrl, requestData, null, addToRequestQueue);
                    break;
                case RequestType.Put:
                    PutAsync(requestUrl, requestData, null, addToRequestQueue);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        
        private string BuildRequestParams()
        {
            var requestStringBuilder = new StringBuilder();
            //Metrics added to each request
            foreach (var item in _pdUtils.GetBaseParams())
            {
                requestStringBuilder.AppendFormat("/{0}", Convert.ToString(item.Value)); 
            }
            
            return requestStringBuilder.ToString();
        }
        
        internal void SendRequestAsync(string url, RequestType requestType, object body,
            bool addToRequestQueue = false)
        {
            var json = ToJson(body);
            ProcessRequest(requestType, url, json, addToRequestQueue);
        }

        internal void GetAsync(string uri, Action<PdResponse> response = null, bool addToRequestQueue = false)
        {
            var parameters = BuildRequestParams();
            var url = uri + parameters;
            var request = UnityWebRequest.Get(url);
            var pdRequestModel = addToRequestQueue ? 
                new PdRequestModel(RequestType.Get, uri, null, DateTime.UtcNow) 
                : null;
            SendRequest(request, response, pdRequestModel);
        }

        public void PostAsync(string uri, object data, Action<PdResponse> response = null, bool addToRequestQueue = false)
        {
            var json = ToJson(data);
            PostAsync(uri, json, response, addToRequestQueue);
        }
        
        
        public void PostAsync(string uri, string json, Action<PdResponse> response = null, bool addToRequestQueue = false)
        {
            var parameters = BuildRequestParams();
            var url = uri + parameters;
            Debug.Log("PostAsync: " + url);
            var request = UnityWebRequest.Post(url, new WWWForm());
            UploadHandler uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(json));
            uploadHandler.contentType = "application/json";
            request.uploadHandler = uploadHandler;
            var pdRequestModel = addToRequestQueue ? 
                new PdRequestModel(RequestType.Post, uri, json, DateTime.UtcNow) 
                : null;
            SendRequest(request, response, pdRequestModel);
        }
        
        public void PutAsync(string uri, string json, Action<PdResponse> response = null, bool addToRequestQueue = false)
        {
            var parameters = BuildRequestParams();
            var url = uri + parameters;
            var request = UnityWebRequest.Put(url, json);
            UploadHandler uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(json));
            uploadHandler.contentType = "application/json";
            request.uploadHandler = uploadHandler;
            var pdRequestModel = addToRequestQueue ? 
                new PdRequestModel(RequestType.Put, uri, json, DateTime.UtcNow) 
                : null;
            SendRequest(request, response, pdRequestModel);
        }
        

        private void SendRequest(UnityWebRequest request, Action<PdResponse> response = null, PdRequestModel pdRequestModel = null)
        {
            var async = request.SendWebRequest();
            Debug.Log("SendRequest: " + request.url);
            async.completed += operation =>
            {
                Debug.Log("Response, isNetworkError: " + request.isNetworkError);
                var pdResponse = new PdResponse {IsNetworkError = request.isNetworkError};
                if (request.isNetworkError)
                {
                    if (pdRequestModel != null)
                    {
                        AddRequestToQueue(pdRequestModel);
                    }
                    pdResponse.ErrorMessage = "Network error";
                    
                }
                else
                {
                    var text = request.downloadHandler.text;
                    pdResponse.StatusCode = request.responseCode;
                    pdResponse.Data = text;
                    pdResponse.ErrorMessage = request.error;
                }
                response?.Invoke(pdResponse);
            };
        }
        
        internal async Task<PdResponse> GetTaskAsync(string uri, bool withPdParams = true, bool addToRequestQueue = false)
        {
            var url = uri;
            if (withPdParams)
            {
                var parameters = BuildRequestParams();
                url = uri + parameters;
            }
            
            var pdResponse = new PdResponse();
            try
            {
                Debug.Log("GetTaskAsync: " + url);
                var request = (HttpWebRequest) WebRequest.Create(url);
                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
                {
                    var res = await reader.ReadToEndAsync();
                    pdResponse.Data = res;

                    return pdResponse;
                }
            }
            catch (Exception ex)
            {
                pdResponse.ErrorMessage = ex.Message;
            }

            if (addToRequestQueue)
            {
                var requestModel = new PdRequestModel(RequestType.Get, url, null, DateTime.UtcNow);
                AddRequestToQueue(requestModel);              
            }
            
            return pdResponse;
        }

        public async Task<PdResponse> PostTaskAsync(string uri, object data, bool withPdParams = true, bool addToRequestQueue = false)
        {
            var json = ToJson(data);
            var url = uri;
            if (withPdParams)
            {
                var parameters = BuildRequestParams();
                url = uri + parameters;
            }

            var pdResponse = new PdResponse();
            try
            {
                Debug.Log("PostTaskAsync: " + url);
                var dataBytes = Encoding.UTF8.GetBytes(json);

                var request = (HttpWebRequest) WebRequest.Create(url);
                request.ContentLength = dataBytes.Length;
                request.ContentType = "application/json";
                request.Method = "POST";


                using (var requestBody = request.GetRequestStream())
                {
                    await requestBody.WriteAsync(dataBytes, 0, dataBytes.Length);
                }

                using (var response = (HttpWebResponse) await request.GetResponseAsync())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
                {
                    var res = await reader.ReadToEndAsync();
                    pdResponse.Data = res;
                    
                    return pdResponse;
                }
            }
            catch (Exception ex)
            {
                pdResponse.ErrorMessage = ex.Message;
            }

            if (addToRequestQueue)
            {
                var requestModel = new PdRequestModel(RequestType.Post, url, json, DateTime.UtcNow);
                AddRequestToQueue(requestModel);              
            }
            
            return pdResponse;
        }
        
        private string ToJson(object body)
        {
            return JsonConvert.SerializeObject(body, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }
    }
}