using UnityEngine;

namespace Plugins.PdAnalytics.Helpers
{
	public static class UserUidHelper
	{
		public static long? GetUserUid()
		{
			if (PlayerPrefs.HasKey(Constants.UserUidKey))
			{
				var userUidStr = PlayerPrefs.GetString(Constants.UserUidKey);
				return long.Parse(userUidStr);
			}
			return null;
		}

		public static void SaveUserUid(long userUid)
		{
			PlayerPrefs.SetString(Constants.UserUidKey, userUid.ToString());
			PlayerPrefs.Save();
		}
	}
}