﻿namespace Plugins.PdAnalytics.Helpers
{
    public struct PdResponse
    {
        public long StatusCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Data { get; set; }
        public bool IsNetworkError { set; get; }

        public bool IsSuccess => string.IsNullOrEmpty(ErrorMessage);

        public override string ToString()
        {
            return $"{nameof(StatusCode)}: {StatusCode}, {nameof(ErrorMessage)}: {ErrorMessage}, {nameof(Data)}: {Data}, {nameof(IsNetworkError)}: {IsNetworkError}, {nameof(IsSuccess)}: {IsSuccess}";
        }
    }
}
