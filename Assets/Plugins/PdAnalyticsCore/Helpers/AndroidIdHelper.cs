using UnityEngine;

namespace Plugins.PdAnalytics.Helpers
{
	internal static class AndroidIdHelper
	{
		public static string GetAndroidId()
		{
#if UNITY_ANDROID && !UNITY_EDITOR
			return GetAndroidIdFromJava();
#endif
			return string.Empty;
		}

		private static string GetAndroidIdFromJava()
		{
			var clsUnity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			var objActivity = clsUnity.GetStatic<AndroidJavaObject>("currentActivity");
			var objResolver = objActivity.Call<AndroidJavaObject>("getContentResolver");
			var clsSecure = new AndroidJavaClass("android.provider.Settings$Secure");
			var androidId = clsSecure.CallStatic<string>("getString", objResolver, "android_id");
			Debug.Log("[AndroidIdHelper] AndroidId: " + androidId);
			return androidId;
		}
	}
}