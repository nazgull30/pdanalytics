using System.Collections.Generic;
using Newtonsoft.Json;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Entities;

namespace Plugins.PdAnalytics.Helpers
{
    public static class Converter
    {
        public static PdEventModel ConvertEventEntityToEventModel(EventEntity entity)
        {
            var model = JsonConvert.DeserializeObject<PdEventModel>(entity.Json);
            model.Id = entity.Id;
            return model;
        } 
        
        public static EventEntity ConvertEventModelToEventEntity(PdEventModel model, long id)
        {
            var json = JsonConvert.SerializeObject(model, Formatting.Indented,
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            model.Id = id;
            
            return new EventEntity
            {
                Id = id,
                Json = json
            };
        }
        
        public static SegmentModel ConvertSegmentEntityToSegmentModel(SegmentEntity entity)
        {
            var model = JsonConvert.DeserializeObject<SegmentModel>(entity.Json);
            model.Id = entity.Id;
            return model;
        }

        public static SegmentEntity ConvertSegmentModelToSegmentEntity(SegmentModel model, long id)
        {
            var json = JsonConvert.SerializeObject(model, Formatting.Indented,
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            model.Id = id;
            
            return new SegmentEntity
            {
                Id = id,
                Json = json
            };
        }
        
        public static PdRequestModel ConvertRequestEntityToRequestModel(RequestEntity entity)
        {
            var model = JsonConvert.DeserializeObject<PdRequestModel>(entity.Json);
            model.Id = entity.Id;
            return model;
        }

        public static RequestEntity ConvertRequestModelToRequestEntity(PdRequestModel model, long id)
        {
            var json = JsonConvert.SerializeObject(model, Formatting.Indented,
                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore});
            model.Id = id;
            
            return new RequestEntity
            {
                Id = id,
                Json = json
            };
        }

        public static Dictionary<string, object> ConvertJsonToDictionary(string json)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
        }
    }
}