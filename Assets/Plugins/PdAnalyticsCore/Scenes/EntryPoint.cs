﻿using System.Collections;
using Plugins.PdAnalytics.Models;
using UnityEngine;

namespace Plugins.PdAnalytics.Scenes
{
    public class EntryPoint : MonoBehaviour
    {
        public PdAnalytics.Impl.PdAnalytics pdAnalyticsPrefab;
    
        private void Awake()
        {
            var pdAnalytics = Instantiate(pdAnalyticsPrefab);

            StartCoroutine(SendEvents(pdAnalytics));


        }

        private IEnumerator SendEvents(IPdAnalytics pdAnalytics)
        {
            yield return new WaitForSeconds(1);
            
            // pdAnalytics.Events.RecordEventAsync("ad.impression.vast", new SegmentModel
            // {
            //     {"type", "video"},
            //     {"state", "Complete"},
            //     {"error", ""},
            //     {"sdk", "adMob"},
            //     {"place", "ExpandInventory"},
            // }, true, 1, 0, 30);
            
            yield return new WaitForSeconds(1);
            
            
            // pdAnalytics.Events.RecordEventAsync("ad.impression.vast", new SegmentModel
            // {
            //     {"type", "video"},
            //     {"state", "Complete"},
            //     {"error", ""},
            //     {"sdk", "adMob"},
            //     {"place", "Revive"},
            // }, true, 1, 0, 30);
            
            yield return new WaitForSeconds(1);
            //
            // pdAnalytics.Events.RecordEventAsync("ad.impression.vast", new SegmentModel
            // {
            //     {"type", "video"},
            //     {"state", "Complete"},
            //     {"error", ""},
            //     {"sdk", "adMob"},
            //     {"place", "UnlockSlot"},
            // }, true, 1, 0, 30);
            
            
            
            // pdAnalytics.UserDetails.Set("name", "Mark");
            // pdAnalytics.UserDetails.Set("age", 32);
            // pdAnalytics.UserDetails.Set("school", 0);
            // pdAnalytics.UserDetails.SaveAsync();
            
            // pdAnalytics.Events.RecordEventAsync("craft_item", new SegmentModel
            // {
            //     {"slot_index", 0},
            //     {"station_type", "Regular"},
            //     {"item_type", "Axe"},
            // }, true, 1, -10, 30);
            //
            // yield return new WaitForSeconds(1);

            // pdAnalytics.Balance.RecordChangeVirtualCurrencyBalance("Coins", 100, "InstantCraft");
            //
            // pdAnalytics.Events.RecordEventAsync("craft_item_instant", new SegmentModel
            // {
            //     {"slot_index", 1},
            //     {"station_type", "Regular"},
            //     {"item_type", "Pickaxe"},
            //     {"action_source_type", "Coins"},
            //     {"price", 10},
            // }, true, 1, 10, 30);
            
            yield return new WaitForSeconds(1);
            
            // yield return new WaitForSeconds(1);
            //
            RecordPurchase(pdAnalytics);
        }

        private void RecordPurchase(IPdAnalytics pdAnalytics)
        {
            var purchaseModel = new PdPurchaseModel
            {
                ProductId = "coins.pack1",
                TransactionId = "GPA.3394-8641-5320-93425",
                Token = "cmnhmohdokiijjeifimilfob.AO-J1OyVhXpJl6nvRmEbXYnhPc3qCU_OaBxbZsxy9-ibh4oL-h1Fj0Peif5Zg3oTlkAocrCYMZm5wpGWKuX6uZYYSKGGId0TGAQN5WO4NAatYsf2fHd779E17lD_Q93jF7mtyopiRwMpNwRa1qVD0bDjbqJiZW2AgoGIv0OqpZgP-X5R2Y05-sQ",
                Price = 300,
                CurrencyCode = "RUB",
                Quantity = 1
            };
            pdAnalytics.Purchases.RecordPurchase(purchaseModel, r =>
            {
                Debug.Log(r.IsSuccess);
                Debug.Log(r.ErrorMessage); 
            });
        }
        
    }
}
