using Newtonsoft.Json;

namespace Plugins.PdAnalytics.Models
{
    internal class PdSessionModel
    {
        [JsonProperty("aid")] public string AndroidId { get; set; }
        [JsonProperty("ts")] public long Timestamp { get; set; }
        [JsonProperty("metrics")] public PdMetricModel Metrics { get; set; }
    }
}