﻿using System;
using Plugins.PdAnalytics.Persistence;

namespace Plugins.PdAnalytics.Models
{
    public class PdRequestModel : IModel
    {
        public PdRequestModel(RequestType requestType, string requestUrl, string requestData,
            DateTime requestDateTime)
        {
            RequestType = requestType;
            RequestUrl = requestUrl;
            RequestData = requestData;
            RequestDateTime = requestDateTime;
        }

        public RequestType RequestType { get; set;  }
        public string RequestUrl { get; set;  }
        public string RequestData { get; set;  }
        public DateTime RequestDateTime { get; set; }
        public long Id { get; set; }
    }

    public enum RequestType
    {
        Get, Post, Put 
    }
}