using Newtonsoft.Json;

namespace Plugins.PdAnalytics.Models
{
    public class PdPurchaseModel
    {
        [JsonProperty("pid")] public string ProductId { get; set; }

        [JsonProperty("token")] public string Token { get; set; }
        
        [JsonProperty("tr")] public string TransactionId { get; set; }

        [JsonProperty("cr")] public string CurrencyCode { get; set; }

        [JsonProperty("pr")] public double Price { get; set; }
        
        [JsonProperty("q")] public long Quantity { get; set; }
        
        [JsonProperty("appv")] public string AppVersion { get; set; }

        public override string ToString()
        {
            return $"";
        }
    }
}