using Newtonsoft.Json;

namespace Plugins.PdAnalytics.Models
{
    public class UserPropertyUpdate
    {
        [JsonProperty("key")] 
        public string Key { set; get; }
        
        [JsonProperty("upd")]
        public Parameters Update { set; get; }
        
        public class Parameters
        {
            [JsonProperty("op")] 
            public Operator Operator { set; get; }

            [JsonProperty("v")]
            public object Value;
        }
        
        public enum Operator {
            Set, SetOnce, Inc, Mul, Max, Min
        }
    }
    

}