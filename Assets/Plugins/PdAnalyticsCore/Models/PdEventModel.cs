﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Plugins.PdAnalytics.Persistence;
using UnityEngine;

namespace Plugins.PdAnalytics.Models
{
    [Serializable]
    public class PdEventModel : IModel
    {
        /// <summary>
        ///     Initializes a new instance of event model.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="segmentation"></param>
        /// <param name="count"></param>
        /// <param name="sum"></param>
        /// <param name="duration"></param>
        public PdEventModel(string key, IDictionary<string, object> segmentation = null, int? count = 1,
            double? sum = null,
            double? duration = null)
        {
            Key = key;
            Count = count ?? 1;
            if (segmentation != null)
            {
                Segmentation = new SegmentModel(segmentation);
            }
            Duration = duration;
            Sum = sum;
            
            Timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            Timezone = TimeZoneInfo.Local.BaseUtcOffset.TotalMilliseconds;
            AppVersion = Application.version;
        }

        public PdEventModel()
        {
        }

        [JsonIgnore]
        public long Id { get; set; }
        
        [JsonProperty("key")] public string Key { get; set; }

        [JsonProperty("count")] public int? Count { get; set; }

        [JsonProperty("sum")] public double? Sum { get; set; }

        [JsonProperty("dur")] public double? Duration { get; set; }

        [JsonProperty("seg")] public SegmentModel Segmentation { get; set; }

        [JsonProperty("ts")] public long Timestamp { get; set; }

        [JsonProperty("tz")] public double Timezone { get; set; }
        
        [JsonProperty("appv")] public string AppVersion { get; set; }

//        [JsonIgnore] public DateTime TimeRecorded { get; set; }

        #region Reserved Event Names

        [JsonIgnore] internal const string ViewEvent = "[CLY]_view";

        [JsonIgnore] internal const string ViewActionEvent = "[CLY]_action";

        [JsonIgnore] internal const string StarRatingEvent = "[CLY]_star_rating";

        [JsonIgnore] internal const string PushActionEvent = "[CLY]_push_action";

        #endregion

        protected bool Equals(PdEventModel other)
        {
            return Id == other.Id && Key == other.Key && Count == other.Count && Nullable.Equals(Sum, other.Sum) && Nullable.Equals(Duration, other.Duration) && Equals(Segmentation, other.Segmentation) && Timestamp == other.Timestamp && Timezone.Equals(other.Timezone) && AppVersion == other.AppVersion;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((PdEventModel) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id.GetHashCode();
                hashCode = (hashCode * 397) ^ (Key != null ? Key.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Count.GetHashCode();
                hashCode = (hashCode * 397) ^ Sum.GetHashCode();
                hashCode = (hashCode * 397) ^ Duration.GetHashCode();
                hashCode = (hashCode * 397) ^ (Segmentation != null ? Segmentation.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Timestamp.GetHashCode();
                hashCode = (hashCode * 397) ^ Timezone.GetHashCode();
                hashCode = (hashCode * 397) ^ (AppVersion != null ? AppVersion.GetHashCode() : 0);
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"{nameof(Id)}: {Id}, {nameof(Key)}: {Key}, {nameof(Count)}: {Count}, {nameof(Sum)}: {Sum}, {nameof(Duration)}: {Duration}, {nameof(Segmentation)}: {Segmentation}, {nameof(Timestamp)}: {Timestamp}, {nameof(Timezone)}: {Timezone}, {nameof(AppVersion)}: {AppVersion}";
        }
    }
}