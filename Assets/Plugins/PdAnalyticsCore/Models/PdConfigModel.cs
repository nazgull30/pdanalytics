﻿using System;

namespace Plugins.PdAnalytics.Models
{
    [Serializable]
    public class PdConfigModel
    {
        public bool EnableFirstAppLaunchSegment;
        public bool EnableManualSessionHandling;
        public int SessionDuration;
        public int StoredRequestLimit;
        public int TotalBreadcrumbsAllowed;
        public bool EnableAutomaticCrashReporting;
        
        public PdConfigModel(bool enableManualSessionHandling = false,
                                    int sessionDuration = 60, 
                                    int storedRequestLimit = 1000, int totalBreadcrumbsAllowed = 100,
                                    bool enableAutomaticCrashReporting = true)

        {
            SessionDuration = sessionDuration;
            EnableManualSessionHandling = enableManualSessionHandling;
            StoredRequestLimit = storedRequestLimit;
            TotalBreadcrumbsAllowed = totalBreadcrumbsAllowed;
            EnableAutomaticCrashReporting = enableAutomaticCrashReporting;
        }

    }
}
