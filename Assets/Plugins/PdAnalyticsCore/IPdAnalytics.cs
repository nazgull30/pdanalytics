using Plugins.PdAnalytics.Services;
using Plugins.PdAnalytics.Services.Impls;

namespace Plugins.PdAnalytics
{
    public interface IPdAnalytics
    {
        ICrushReportsPdService CrushReports { get; }
        IDeviceIdPdService Device { get; }
        IEventPdService Events { get; }
        IInitializationPdService Initialization { get; }
        IOptionalParametersPdService OptionalParameters { get; }
        IRemoteConfigPdService RemoteConfigs { get; }
        IStarRatingPdService StarRating { get; }
        IUserDetailsPdService UserDetails { get; }
        IViewPdService Views { get; }
        IPurchasePdService Purchases { get; }
        IBalancePdService Balance { get; }

        void ReportAll();
    }
}