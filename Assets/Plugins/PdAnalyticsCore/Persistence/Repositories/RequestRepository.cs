using Plugins.iBoxDB;
using Plugins.PdAnalytics.Helpers;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Entities;

namespace Plugins.PdAnalytics.Persistence.Repositories
{
    internal class RequestRepository : Repository<RequestEntity, PdRequestModel>
    {
        public RequestRepository(Dao<RequestEntity> dao) : base(dao)
        {
        }

        protected override PdRequestModel ConvertEntityToModel(RequestEntity entity)
        {
            return Converter.ConvertRequestEntityToRequestModel(entity);
        }

        protected override RequestEntity ConvertModelToEntity(PdRequestModel model)
        {
            return Converter.ConvertRequestModelToRequestEntity(model, GenerateNewId());
        }

        protected override bool ValidateModelBeforeEnqueue(PdRequestModel model)
        {
            return true;
        }
    }
}