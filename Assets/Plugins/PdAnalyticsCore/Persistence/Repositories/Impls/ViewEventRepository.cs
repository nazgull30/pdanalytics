using Plugins.iBoxDB;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Dao;
using Plugins.PdAnalytics.Persistence.Entities;
using UnityEngine;

namespace Plugins.PdAnalytics.Persistence.Repositories.Impls
{
    public class ViewEventRepository : AbstractEventRepository
    {
        public ViewEventRepository(Dao<EventEntity> dao, SegmentDao segmentDao) : base(dao, segmentDao)
        {
        }

        protected override bool ValidateModelBeforeEnqueue(PdEventModel model)
        {
            Debug.Log("[ViewEventRepository] Validate model: \n" + model);
            return model.Key.Equals(PdEventModel.ViewEvent);
        }
    }
}