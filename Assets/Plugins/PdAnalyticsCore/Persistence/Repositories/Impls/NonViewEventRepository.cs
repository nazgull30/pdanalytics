using Plugins.iBoxDB;
using Plugins.PdAnalytics.Models;
using Plugins.PdAnalytics.Persistence.Dao;
using Plugins.PdAnalytics.Persistence.Entities;

namespace Plugins.PdAnalytics.Persistence.Repositories.Impls
{
    public class NonViewEventRepository : AbstractEventRepository
    {
        public NonViewEventRepository(Dao<EventEntity> dao, SegmentDao segmentDao) : base(dao, segmentDao)
        {
        }

        protected override bool ValidateModelBeforeEnqueue(PdEventModel model)
        {
            return !model.Key.Equals(PdEventModel.ViewEvent);
        }
    }
}