using Plugins.iBoxDB;

namespace Plugins.PdAnalytics.Persistence.Entities
{
    public class EventNumberInSameSessionEntity : IEntity
    {
        public long Id;
        public string EventKey;
        public int Number;

        public long GetId()
        {
            return Id;
        }
    }
}