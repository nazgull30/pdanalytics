namespace Plugins.PdAnalytics.Persistence
{
    public interface IModel
    {
        long Id { set; get; }
    }
}