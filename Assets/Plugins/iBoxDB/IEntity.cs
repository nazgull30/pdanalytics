namespace Plugins.iBoxDB
{
    public interface IEntity
    {
        long GetId();
    }
}